## Описание проекта

Проект по автоматизации тестирования API ресурса restful-booker на курсе "Python QA Engineer " от OTUS.

Restful-booker - открытое API для тренировки работы с запросами.

[Ссылка на документацию API restful-booker](https://restful-booker.herokuapp.com/apidoc/index.html) 

+ В директории tests/ находятся файлы с тестами на методы API. Каждый файл - на отдельный метод.
  
  - tests/test_create_booking.py - CreateBooking
  - tests/test_delete_booking.py - DeleteBooking
  - tests/test_get_booking.py - GetBooking
  - tests/test_get_booking_ids.py - GetBookingIds
  - tests/test_partial_update_booking.py - PartialUpdateBooking
  - tests/test_update_booking.py - UpdateBooking

____

## Установка проекта

- Скачать репозиторий на свою машину:

```
git clone https://gitlab.com/autotests2384809/otus_project_restful_booker.git
```

- Перейти в директорию скачанного репозитория

- Установить и активировать виртуальное окружение (для запуска тестов через консоль/ide)

```
python3 -m venv venv
source venv/bin/activate
```
- Обновить PIP и установить зависимости (для запуска тестов через консоль/ide)

```
pip install -U pip
pip install -r requirements.txt
```

- Выбрать интерпретатор для проекта (для запуска тестов через консоль/ide)
____

## Запуск тестов

### __В IDE PyCharm__

#### Запустить тесты в PyCharm 

В Terminal выполнить команду:

```
pytest -n 2 -m marker tests/
```
где:

- -n - во сколько потоков запускать тесты, если не указывать параметр при запуске - тесты будут запущены в 1 поток.
- -m - маркер, какую группу тестов запускать.
Варианты: create_booking, delete_booking, get_booking, get_booking_ids, patch_booking, put_booking
если не добавлять маркер - запускаются все тесты
Дополнительные параметры (см. config.yml):
- --schema=http|https - по-умолчанию https
- --host - по-умолчанию default (restful-booker.herokuapp.com)
- --login- по-умолчанию login
- --passw- по-умолчанию password



#### Получить отчет Allure 

- В Terminal выполнить команду, в качестве параметра указав путь до исполняемого файла allure на вашей машине:

```
./run_allure_report.sh /path/to/allure/bin
Пример: ./run_allure_report.sh /Applications/allure/bin/allure
```